import { SlideshowFactory } from './../../src/bootstrap.js'

const options = SlideshowFactory.getOptionsAdapter().getTestOptions()

describe('Slideshow', () => {
	beforeEach(() => {
		cy.clock()
	})

	describe('default slideshow', () => {
		beforeEach(() => {
			cy.visit('/slideshow-1.html')
		})

		it ('displays only first slide', () => {
			cy.get(options.activeSlideClass).should('contain', 'Image 1')
		})

		it ('displays prev button', () => {
			cy.get('[data-cy="prev"]').should('exist')
		})

		it ('displays next button', () => {
			cy.get('[data-cy="next"]').should('exist')
		})

		it ('displays pagination', () => {
			cy.get('[data-cy="pagination"] *').should('have.length', 3)
		})

		it ('displays next', () => {
			cy.get(options.activeSlideClass).should('contain', 'Image 1')
			cy.get('[data-cy="next"]').click()
			cy.tick(options.transitionDuration)
			cy.get(options.activeSlideClass).should('contain', 'Image 2')
		})

		it ('displays previous', () => {
			cy.get(options.activeSlideClass).should('contain', 'Image 1')
			cy.get('[data-cy="prev"]').click()
			cy.tick(options.transitionDuration)
			cy.get(options.activeSlideClass).should('contain', 'Image 3')
		})

		it ('autoplays', () => {
			cy.get(options.activeSlideClass).should('contain', 'Image 1')
			cy.tick(options.autoplay)
			cy.tick(options.transitionDuration)
			cy.get(options.activeSlideClass).should('contain', 'Image 2')
		})
	})

	context('slideshow with custom options', () => {
		beforeEach(() => {
			cy.visit('/slideshow-2.html')
		})

		it ('does not autoplay if specified in options', () => {
			cy.get(options.activeSlideClass).should('contain', 'Image 1')
			cy.tick(options.autoplay)
			cy.tick(options.transitionDuration)
			cy.get(options.activeSlideClass).should('contain', 'Image 1')
		})
	})
})