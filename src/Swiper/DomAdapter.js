import Swiper, { Navigation, Pagination, Autoplay } from 'swiper';

Swiper.use([ Navigation, Pagination, Autoplay ]);

class DomAdapter
{
	constructor(slideshow, images) {
		this.slideshow = slideshow;
		this.images = images;

		this.container = this.createContainer()
		this.wrapper = this.createWrapper()
		this.arrows = this.createArrows()
		this.pagination = this.createPagination()

		this.createDomTree()
	}

	mount(options) {
		this.attach()

		options = Object.assign(options, {
			pagination: {
			    el: '.swiper-pagination',
			    clickable: true
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		})

		let swiper = new Swiper(this.container, options);
	}

	attach() {
		this.migrateImages()
		
		this.slideshow.parentElement.insertBefore(
			this.container, 
			this.slideshow
		)

		this.slideshow.parentElement.removeChild(this.slideshow)
	}

	createDomTree() {
		this.container.append(this.wrapper)
		this.container.append(this.arrows[0])
		this.container.append(this.arrows[1])
		this.container.append(this.pagination)
	}

	createContainer() {
		let container = document.createElement("div")
		container.className = 'swiper'
		return container
	}

	createWrapper() {
		let track = document.createElement('div')
		track.className = 'swiper-wrapper'
		return track;
	}

	createArrows() {
		return [
			this.createPreviousArrow(),
			this.createNextArrow(),
		]
	}

	createPagination() {
		let pagination = document.createElement('div')
		pagination.className = 'swiper-pagination'
		pagination.setAttribute('data-cy', 'pagination')
		return pagination
	}

	migrateImages() {
		let i = 0
		while (this.images.length > 0) {
			this.createSlide()
			i++
		}
	}

	createSlide() {
		let slide = document.createElement('div')
		slide.className = 'swiper-slide'
		slide.appendChild(this.images[0])
		this.wrapper.append(slide)
	}

	createPreviousArrow() {
		let prev = document.createElement('div')
		prev.className = 'swiper-button-prev'
		prev.setAttribute('data-cy','prev')
		return prev
	}

	createNextArrow() {
		let next = document.createElement('div')
		next.className = 'swiper-button-next'
		next.setAttribute('data-cy','next')
		return next
	}
}

export default DomAdapter