import OptionsAdapter from './OptionsAdapter'
import DomAdapter from './DomAdapter'

class Adapter
{
	getOptionsAdapter() {
		return new OptionsAdapter()
	}

	getDomAdapter(slideshow, images) {
		return new DomAdapter(slideshow, images)
	}
}

export default Adapter