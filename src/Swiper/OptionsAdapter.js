class OptionsAdapter
{
	getDefaultOptions() {
		return {
			autoplay: {
				delay: 4000,
				disableOnInteraction: false
			},
			loop: true
		}
	}

	getTestOptions() {
		return {
			autoplay: 4000,
			transitionDuration: 1000,
			activeSlideClass: '.swiper-slide-active'
		}
	}
}

export default OptionsAdapter