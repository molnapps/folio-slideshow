class SlideshowFactory
{
	constructor() {
		this.adapter = null
	}

	use(adapter) {
		this.adapter = new adapter()
	}
	
	getOptionsAdapter() {
		return this.adapter.getOptionsAdapter()
	}

	getDomAdapter(slideshow, images) {
		return this.adapter.getDomAdapter(slideshow, images)
	}
}

export default new SlideshowFactory()