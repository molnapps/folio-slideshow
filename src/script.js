import { SlideshowFactory } from './bootstrap'

window.addEventListener('DOMContentLoaded', (event) => {
	let slideshows = document.getElementsByClassName('Slideshow')

	while (slideshows.length > 0) {
		let images = slideshows[0].getElementsByTagName('div')
		let customOptions = JSON.parse(slideshows[0].getAttribute('data-options'));
		
		let container = SlideshowFactory.getDomAdapter(slideshows[0], images).mount(
	    	Object.assign(
	    		SlideshowFactory.getOptionsAdapter().getDefaultOptions(), 
	    		customOptions
	    	)
		)
	}
})