class OptionsAdapter
{
	getDefaultOptions() {
		return {
			type: 'carousel',
			gap: 0,
			hoverpause: false,
			autoplay: 4000,
			animationDuration: 1000
		}
	}

	getTestOptions() {
		return {
			autoplay: 4000,
			transitionDuration: 1000,
			activeSlideClass: '.glide__slide--active'
		}
	}
}

export default OptionsAdapter