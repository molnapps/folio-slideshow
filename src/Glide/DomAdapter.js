import Glide from '@glidejs/glide'

class DomAdapter
{
	constructor(slideshow, images) {
		this.slideshow = slideshow;
		this.images = images;

		this.container = this.createContainer()
		this.track = this.createTrack()
		this.slides = this.createSlides()
		this.arrows = this.createArrows()
		this.bullets = this.createBullets()

		this.createDomTree()
	}

	mount(options) {
		this.attach()

		new Glide(this.container,  options).mount()
	}

	attach() {
		this.migrateImages()
		
		this.slideshow.parentElement.insertBefore(
			this.container, 
			this.slideshow
		)

		this.slideshow.parentElement.removeChild(this.slideshow)
	}

	createDomTree() {
		this.createArrowButtons()
		this.container.append(this.track)
		this.track.append(this.slides)
		this.track.append(this.arrows)
		this.track.append(this.bullets)
	}

	createContainer() {
		let container = document.createElement("div")
		container.className = 'glide'
		return container
	}

	createTrack() {
		let track = document.createElement('div')
		track.className = 'glide__track'
		track.setAttribute('data-glide-el','track')
		return track;
	}

	createSlides() {
		let slides = document.createElement('div')
		slides.className = 'glide__slides'
		return slides
	}

	createArrows() {
		let arrows = document.createElement('div')
		arrows.className = 'glide__arrows'
		arrows.setAttribute('data-glide-el','controls')
		return arrows
	}

	createBullets() {
		let bullets = document.createElement('div')
		bullets.className = 'glide__bullets'
		bullets.setAttribute('data-glide-el','controls[nav]');
		bullets.setAttribute('data-cy','pagination');
		return bullets
	}

	migrateImages() {
		let i = 0
		while (this.images.length > 0) {
			this.createSlide()
			this.createBullet(i)
			i++
		}
	}

	createSlide() {
		let slide = document.createElement('div')
		slide.className = 'glide__slide'
		slide.appendChild(this.images[0])
		this.slides.append(slide)
	}

	createBullet(i) {
		let bullet = document.createElement('button')
		bullet.className = 'glide__bullet'
		bullet.setAttribute('data-glide-dir', '=' + i)
		this.bullets.appendChild(bullet)
	}

	createArrowButtons() {
		this.createPreviousArrowButton()
		this.createNextArrowButton()
	}

	createPreviousArrowButton() {
		let prevButton = document.createElement('button')
		prevButton.className = 'glide__arrow glide__arrow--left'
		prevButton.setAttribute('data-glide-dir', '<')
		prevButton.setAttribute('data-cy', 'prev')
		prevButton.innerText = 'Prev'
		this.arrows.append(prevButton)
	}

	createNextArrowButton() {
		let nextButton = document.createElement('button')
		nextButton.className = 'glide__arrow glide__arrow--right'
		nextButton.setAttribute('data-glide-dir', '>')
		nextButton.setAttribute('data-cy', 'next')
		nextButton.innerText = 'Next'
		this.arrows.append(nextButton)
	}
}

export default DomAdapter