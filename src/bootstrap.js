import SlideshowFactory from './SlideshowFactory'
import SwiperAdapter from './Swiper/Adapter'

SlideshowFactory.use(SwiperAdapter)

export { SlideshowFactory }
export default SlideshowFactory